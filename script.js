function renderMovies(film, charsContainer) {
  const titles = document.createElement('h3');
  const shortPlot = document.createElement('p');
  titles.innerHTML = `Episode ${film.episodeId}__ ${film.name}`;
  shortPlot.innerHTML = `${film.openingCrawl}`;

  document.body.append(titles, shortPlot, charsContainer);
}

function renderChars(char, charsContainer) {
  const charName = document.createElement('p');
  charName.innerHTML = char.name;
  charsContainer.append(charName);
}

const movies = fetch('https://ajax.test-danit.com/api/swapi/films')
  .then(response => response.json())
  .then(films => {
    films.forEach(film => {
      const charsContainer = document.createElement('div');
      renderMovies(film, charsContainer);

      const chars = film.characters.map(char => fetch(char));
      Promise.all(chars)
        .then(responses => Promise.all(responses.map(res => res.json())))
        .then(chars => chars.forEach(char => renderChars(char, charsContainer)))
    });
  });