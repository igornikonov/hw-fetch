class StarWarsInfo {
  constructor(url, parent) {
    this.url = url;
    this.parent = parent;
  }

  renderMovies({ episodeId, name, openingCrawl }, charsContainer) {
    const titles = document.createElement('h3');
    const shortPlot = document.createElement('p');
    titles.innerHTML = `Episode ${episodeId}__ ${name}`;
    shortPlot.innerHTML = `${openingCrawl}`;

    this.parent.append(titles, shortPlot, charsContainer);
  }

  renderChars(char, charsContainer) {
    const charName = document.createElement('p');
    charName.innerHTML = char.name;
    charsContainer.append(charName);
  }

  render() {
    fetch(this.url)
      .then(response => response.json())
      .then(films => {
        films.forEach(film => {
          const charsContainer = document.createElement('div');
          this.renderMovies(film, charsContainer);

          const chars = film.characters.map(char => fetch(char));
          Promise.all(chars)
            .then(responses => Promise.all(responses.map(res => res.json())))
            .then(chars => chars.forEach(char => this.renderChars(char, charsContainer)))
        });
      });
  }
}

const georgeLucas = new StarWarsInfo('https://ajax.test-danit.com/api/swapi/films', document.querySelector('.root'));

georgeLucas.render();